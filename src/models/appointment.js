const mongoose = require('mongoose')
const moment = require('moment')
const fetch = require("node-fetch");

const appointmentSchema = new mongoose.Schema({
    start: {
        type: String,
        required: true,
        validate(value){
            if(!moment(value,"YYYY-MM-DD HH:mm", true).isValid()){
                throw new Error('Invalid start! ex. 2014-12-13 12:34')
            }
        }
    },
    end:{
        type: String,
        required: true,
        validate(value){
            if(!moment(value, "YYYY-MM-DD HH:mm", true).isValid()){
                throw new Error('Invalid end! ex. 2014-12-13 12:34')
            }
        }
    },
    title: {
        type: String,
        required: true
    },
    priority: {
        type: Number,
        required: true,
        validate(value){
            if(value < 0 || value > 5) throw new Error('priority must be 1-5')
        }
    },
    createby: {
        type: String,
        required: true,
    }
})

appointmentSchema.methods.timeCheck = async function() {
    const appointment = this
    const start = appointment['start']
    const end = appointment['end']
    const startObj = {
        time: start.slice(11),
        date: start.slice(0, 10)
    }
    const endObj = {
        time: end.slice(11),
        date: end.slice(0, 10)
    }
    if(startObj.date > endObj.date) return false
    if(endObj.time <= startObj.time) return false
    else return true
}

appointmentSchema.methods.timeCheckPast = async function() {
    const url = "http://api.timezonedb.com/v2.1/get-time-zone?key=1S1NYUJP5JXF&format=json&by=zone&zone=Asia/Bangkok";
    const appointment = this
    const start = appointment['start']
    const end = appointment['end']
    const getData = async url => {
        try {
            const response = await fetch(url);
            const json = await response.json();
        } catch (error) {
            console.log(error);
        }
    }
    const data = getData(url)
    datetime_now = moment(data.formatted).format("YYYY-MM-DD HH:mm")
    if(start > datetime_now && end > datetime_now) return true
    else return false
}

appointmentSchema.methods.checkDuplicateTime = async function (appointmentList) {
    const appointment = this

    for (let i = 0; i < appointmentList.length; i++) {
        const item = appointmentList[i];
        if (appointment.start >= item.start && appointment.start <= item.end) {
            return true
        } else if (appointment.end >= item.start && appointment.end <= item.end) {
            return true
        } else if (appointment.start <= item.start && appointment.end >= item.end) {
            return true
        }
    }
}

const appointment = mongoose.model('appointment', appointmentSchema)
module.exports = appointment