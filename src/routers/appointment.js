const express = require('express')
const Appointment = require('../models/appointment')
const router = new express.Router()

router.post('/appointments', async (req, res) => {

    try{
        const appointment = new Appointment(req.body)
        const appointmentList = await Appointment.find({
            createby: appointment.createby
        })
        if(!await appointment.timeCheck()) return res.status(404).send({ error:'invalid end must be after start!'})
        if(!await appointment.timeCheckPast()) return res.status(404).send({ error:'invalid datetime can\'t be past!'})
        if(await appointment.checkDuplicateTime(appointmentList)) return res.status(404).send({ error: 'invalid datetime user not avaliable in that datetime'})
        await appointment.save()
        res.status(201).send(appointment)
    }
    catch(e){
        res.status(400).send(e)
    }
})

router.get('/', async (req, res) => {
    try{
        res.render("index");
    }
    catch(e){
        res.status(500).send(e)
    }
})

router.get('/appointments', async (req, res) => {
    try{
        const appointments = await Appointment.find({})
        res.send(appointments)
    }
    catch(e){
        res.status(500).send(e)
    }
})

router.get('/appointments/:id', async (req, res) => {
    const _id = req.params.id

    try{
        const appointment = await Appointment.findById(_id)
        res.send(appointment)
    }
    catch(e){
        if(e.name === 'CastError') return res.status(404).send('ID not found.')
        res.status(500).send(e)
    }
})

router.patch('/appointments/:id', async (req, res) => {
    const updates = Object.keys(req.body)
    const allowedUpdates = ['start', 'end', 'title', 'priority', 'createby']
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))

    if(!isValidOperation) return res.status(404).send({ error: 'invalid updates!' })

    try{
        const appointment = await Appointment.findById(req.params.id)
        updates.forEach((update) => appointment[update] = req.body[update])

        if(!await appointment.timeCheck()) return res.status(404).send({ error:'invalid datetime out must be after datetime in!'})
        if(!await appointment.timeCheckPast()) return res.status(404).send({ error:'invalid datetime out must be after datetime in!'})
        await appointment.save()
        res.send(appointment)
    }
    catch(e){
        if(e.name === 'CastError') return res.status(404).send('ID not found.')
        res.status(500).send(e)
    }
})

router.delete('/appointments/:id', async (req, res) => {
    try{
        const appointment = await Appointment.findByIdAndDelete(req.params.id)
        res.send(appointment)
    }
    catch(e){
        if(e.name === 'CastError') return res.status(404).send('ID not found.')
        res.status(500).send(e)
    }
})

router.get('/appointments/name/:name', async (req, res) => {
    const _name = req.params.name

    try{
        const appointment = await Appointment.find({
            createby: _name
        })
        res.send(appointment)
    }
    catch(e){
        if(e.name === 'CastError') return res.status(404).send('Name not found.')
        res.status(500).send(e)
    }
})


module.exports = router