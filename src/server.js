require('./db/mongoose')
const express = require('express')
const appointmentRouter = require('./routers/appointment')
const cors = require("cors");
const path = require("path")
const app = express()
const port = process.env.port || 3000
app.use(cors())

app.use(cors());
app.set('view engine', 'ejs');
app.set("views", path.join(__dirname, "./templates/views"));
app.use(express.json())
app.use(appointmentRouter)

app.listen(port, () => {
    console.log('Server is up on port ' + port)
})