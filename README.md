# Appointment management
เป็นบริการการทำการนัดหมาย โดยมีลำดับความสำคัญเข้ามาเกี่ยวข้อง

## condition
- start ไม่สามารถจองหลังจาก end ได้
- datetime ไม่สามารถจองเวลาในอดีตได้ ดึง API จาก https://timezonedb.com/api
- priority มีตั้งแต่ 1-5
- ไม่สามารถจองซ้อนทับเวลากันได้

---

## Appointment Entity
```json
{
	"start" : "string",
	"end" : "string",
	"title" : "string",
	"priority" : "number",
	"createby" : "string"
}
```

---

## Appointment Service

### Create
- POST /appointments
```json
{
	"start": "2021-12-13 12:00",
	"end": "2021-12-13 14:00",
	"title": "Arrange a meeting",
	"priority": 5,
	"createby": "Nack Thiti"
}
```

### Read all
- GET /appointments

### Read by ID
- GET /appointments/{id}

### Read by Createby
- GET /appointments/name/{name}

### Update
- PATCH /appointments/{id}
```json
{
	"start": "2021-12-13 12:00",
	"end": "2021-12-13 14:00"
}
```

### Delete
- DELETE /appointments/{id}
